#### Day 1 > Activity 1

## User Registration

Create a program that on boards a user and then sends a code for verification. 

###Flow
1. Create a class `UserRegistration` that will act as the input. Validate the fields.
   1. Fields: `string: email`, `string: password`, `string: firstName`, `string: lastName`, `LocalDate: birthDate`
   2. All fields are required
   3. `email` must be a valid email format
   4. `password` must be at least 5 characters
   5. Age of the user must be at least 18 years old
2. Create a class `User`. Save it to a `List<User>` with generated `User.id`
   1. Generate a unique `string: id`.
   2. Put all fields of `UserRegistration` along with the generated `string: id` inside `User`
   3. Add it to a `List<User>`
3. Create and send a verification code to the `UserRegistration.email`
   1. Generate a `string: code`
   2. Save it to `Map<code, UserRegistration.email>` 
   3. Display log saying `string: code` successfully sent to `UserRegistration.email`